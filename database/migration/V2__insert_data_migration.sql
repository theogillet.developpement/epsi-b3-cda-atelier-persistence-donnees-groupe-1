-- Inserting an admin user for testing
INSERT INTO Users (username, email, password, role)
VALUES ('ADMIN', 'admin@example.com', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 'ADMIN');

-- Inserting seller users for each product
INSERT INTO Users (username, email, password, role)
VALUES ('seller1', 'seller1@example.com', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', 'SELLER');
INSERT INTO Users (username, email, password, role)
VALUES ('seller2', 'seller2@example.com', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', 'SELLER');
INSERT INTO Users (username, email, password, role)
VALUES ('seller3', 'seller3@example.com', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', 'SELLER');
INSERT INTO Users (username, email, password, role)
VALUES ('seller4', 'seller4@example.com', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', 'SELLER');
INSERT INTO Users (username, email, password, role)
VALUES ('seller5', 'seller5@example.com', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', 'SELLER');
INSERT INTO Users (username, email, password, role)
VALUES ('seller6', 'seller6@example.com', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', 'SELLER');
INSERT INTO Users (username, email, password, role)
VALUES ('seller7', 'seller7@example.com', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', 'SELLER');
INSERT INTO Users (username, email, password, role)
VALUES ('seller8', 'seller8@example.com', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', 'SELLER');
INSERT INTO Users (username, email, password, role)
VALUES ('seller9', 'seller9@example.com', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', 'SELLER');

INSERT INTO Users (username, email, password, role)
VALUES ('seller10', 'seller10@example.com', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08',
        'SELLER');

-- Inserting buyer users
INSERT INTO Users (username, email, password, role)
VALUES ('Jean Michel', 'jeanmichel@gmail.com', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08',
        'BUYER');
INSERT INTO Users (username, email, password, role)
VALUES ('buyer2', 'buyer2@example.com', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', 'BUYER');
INSERT INTO Users (username, email, password, role)
VALUES ('buyer3', 'buyer3@example.com', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', 'BUYER');

-- Inserting products
INSERT INTO Products (name, description, price, category, status, seller_id)
VALUES ('Gâteau au chocolat', 'Délicieux gâteau au chocolat fait maison.', 20.00, 'FOOD', 'AVAILABLE', 1);
INSERT INTO Products (name, description, price, category, status, seller_id)
VALUES ('Fauteuil vintage en cuir', 'Fauteuil en cuir véritable, parfait pour une décoration rétro.', 120.00, 'FURNITURE', 'AVAILABLE', 2);
INSERT INTO Products (name, description, price, category, status, seller_id)
VALUES ('Panier de légumes bio', 'Assortiment de légumes frais et biologiques.', 15.00, 'FOOD', 'AVAILABLE', 3);
INSERT INTO Products (name, description, price, category, status, seller_id)
VALUES ('Chaton de race Maine Coon', 'Adorable chaton de race Maine Coon, joueur et affectueux.', 300.00, 'PETS', 'AVAILABLE', 4);
INSERT INTO Products (name, description, price, category, status, seller_id)
VALUES ('Bijoux artisanaux en argent', 'Collection de bijoux faits à la main en argent sterling.', 50.00, 'JEWELRY', 'AVAILABLE', 5);
INSERT INTO Products (name, description, price, category, status, seller_id)
VALUES ('Sac à dos de randonnée', 'Sac à dos spacieux et résistant pour les aventures en plein air.', 80.00, 'SPORTS', 'AVAILABLE', 6);
INSERT INTO Products (name, description, price, category, status, seller_id)
VALUES ('Huile d''olive extra vierge', 'Huile d''olive de première qualité pressée à froid.', 10.00, 'FOOD', 'AVAILABLE', 7);
INSERT INTO Products (name, description, price, category, status, seller_id)
VALUES ('Vélo vintage rénové', 'Vélo vintage entièrement restauré avec un nouveau look.', 200.00, 'SPORTS', 'AVAILABLE', 8);
INSERT INTO Products (name, description, price, category, status, seller_id)
VALUES ('Album de musique vinyle', 'Album classique en vinyle pour les amateurs de musique rétro.', 30.00, 'MUSIC', 'AVAILABLE', 9);
INSERT INTO Products (name, description, price, category, status, seller_id)
VALUES ('Plante d''intérieur', 'Plante d''intérieur facile d''entretien pour embellir votre maison.', 15.00, 'GARDEN', 'AVAILABLE', 10);
