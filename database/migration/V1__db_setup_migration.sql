-- Creating the Users table
CREATE TABLE Users
(
    id       SERIAL PRIMARY KEY,
    username VARCHAR(255)        NOT NULL,
    email    VARCHAR(255) UNIQUE NOT NULL,
    password VARCHAR(255)        NOT NULL,
    role     VARCHAR(255) DEFAULT 'BUYER',
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- Creating the Products table
CREATE TABLE Products
(
    id          SERIAL PRIMARY KEY,
    name        VARCHAR(255)   NOT NULL,
    description TEXT,
    price       DECIMAL(10, 2) NOT NULL,
    category    VARCHAR(255) DEFAULT 'UNCATEGORIZED',
    status      VARCHAR(255)   DEFAULT 'AVAILABLE',
    seller_id   INT,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (seller_id) REFERENCES Users (id) ON DELETE CASCADE
);

-- Creating the Transactions table
CREATE TABLE Transactions
(
    id            SERIAL PRIMARY KEY,
    product_id    INT,
    buyer_id      INT,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (product_id) REFERENCES Products (id) ON DELETE CASCADE,
    FOREIGN KEY (buyer_id) REFERENCES Users (id) ON DELETE CASCADE
);

-- Creating the Ratings table
CREATE TABLE Ratings
(
    id        SERIAL PRIMARY KEY,
    seller_id INT,
    buyer_id  INT,
    rating    INT,
    comment   TEXT,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (seller_id) REFERENCES Users (id) ON DELETE CASCADE,
    FOREIGN KEY (buyer_id) REFERENCES Users (id) ON DELETE CASCADE
);
