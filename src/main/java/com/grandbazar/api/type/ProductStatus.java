package com.grandbazar.api.type;

public enum ProductStatus {
    AVAILABLE("AVAILABLE"),
    SOLD("SOLD");

    private final String status;

    ProductStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
