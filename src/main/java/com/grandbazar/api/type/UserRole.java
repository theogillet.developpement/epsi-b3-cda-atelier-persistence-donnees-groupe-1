package com.grandbazar.api.type;

public enum UserRole {
    ADMIN("ADMIN"),
    SELLER("SELLER"),
    BUYER("BUYER");

    private final String role;

    UserRole(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }
}
