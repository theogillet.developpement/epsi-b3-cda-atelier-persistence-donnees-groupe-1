package com.grandbazar.api.type;

public enum ProductCategory {
    TOYS("TOYS"),
    FOOD("FOOD"),
    ELECTRONICS("ELECTRONICS"),
    CLOTHING("CLOTHING"),
    FURNITURE("FURNITURE"),
    BOOKS("BOOKS"),
    BEAUTY("BEAUTY"),
    SPORTS("SPORTS"),
    JEWELRY("JEWELRY"),
    AUTOMOTIVE("AUTOMOTIVE"),
    MUSIC("MUSIC"),
    PETS("PETS"),
    GARDEN("GARDEN"),
    UNCATEGORIZED("UNCATEGORIZED");

    private final String category;

    ProductCategory(String category) {
        this.category = category;
    }

    public String getCategory() {
        return category;
    }
}
