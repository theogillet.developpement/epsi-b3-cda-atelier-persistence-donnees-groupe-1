package com.grandbazar.api.entity;

import com.grandbazar.api.type.ProductCategory;
import com.grandbazar.api.type.ProductStatus;
import jakarta.persistence.*;
import java.util.Date;

@Entity
@Table(name = "products")
public class ProductEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "price", nullable = false)
    private Double price;

    @Column(name = "created_at", nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt = new Date();

    @Enumerated(EnumType.STRING)
    @Column(name = "category", nullable = false, columnDefinition = "product_category")
    private ProductCategory category = ProductCategory.UNCATEGORIZED;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false, columnDefinition = "product_status")
    private ProductStatus status = ProductStatus.AVAILABLE;

    @Column(name = "seller_id")
    private Integer sellerId;

    public ProductEntity() {}

    public ProductEntity(String name, String description, Double price, Date createdAt, ProductCategory category, ProductStatus status, Integer sellerId) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.createdAt = createdAt != null ? createdAt : new Date();
        this.category = category != null ? category : ProductCategory.UNCATEGORIZED;
        this.status = status != null ? status : ProductStatus.AVAILABLE;
        this.sellerId = sellerId;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public ProductCategory getCategory() {
        return category;
    }

    public void setCategory(ProductCategory category) {
        this.category = category;
    }

    public ProductStatus getStatus() {
        return status;
    }

    public void setStatus(ProductStatus status) {
        this.status = status;
    }

    public Integer getSellerId() {
        return sellerId;
    }

    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }
}
