package com.grandbazar.api.dto;

public record LoginDTO(
        String email,
        String password) {

}
