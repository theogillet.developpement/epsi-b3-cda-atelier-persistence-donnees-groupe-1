package com.grandbazar.api.dto;

import jakarta.validation.constraints.NotNull;
import java.util.Date;

public record TransactionDTO(
        @NotNull(message = "Product ID cannot be null")
        Integer productId,

        @NotNull(message = "Buyer ID cannot be null")
        Integer buyerId,

        Date createdAt
) {}
