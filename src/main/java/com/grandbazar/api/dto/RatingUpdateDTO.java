package com.grandbazar.api.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import java.util.Date;

public record RatingUpdateDTO(
        Integer sellerId,

        Integer buyerId,

        @Min(value = 1, message = "Rating must be at least 1")
        @Max(value = 5, message = "Rating must be at most 5")
        Integer rating,

        String comment,

        Date createdAt
) {}
