package com.grandbazar.api.dto;

import com.grandbazar.api.type.ProductCategory;
import com.grandbazar.api.type.ProductStatus;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.util.Date;

public record ProductUpdateDTO(
        @NotBlank(message = "Product name cannot be blank")
        String name,
        @NotBlank(message = "Product description cannot be blank")
        String description,
        @NotBlank(message = "Product price cannot be blank")
        Double price,

        @NotBlank(message = "Product category cannot be blank")
        ProductCategory category,
        @NotBlank(message = "Product status cannot be blank")
        ProductStatus status,
        @NotNull(message = "Seller id must be given")
        Integer seller_id,

        @NotNull(message = "Le champ createdDate ne peut pas être null")
        Date createdAt
) {

}
