package com.grandbazar.api.dto;

import com.grandbazar.api.type.UserRole;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record UserDTO(
        @NotBlank(message = "Le champ username ne peut pas être vide")
        @NotNull(message = "Le champ username ne peut pas être null")
         String username,
        @NotBlank(message = "Le champ password ne peut pas être vide")
        @NotNull(message = "Le champ password ne peut pas être null")
        String password,
        @NotBlank(message = "Le champ email ne peut pas être vide")
        @NotNull(message = "Le champ email ne peut pas être null")
        String email,

        @NotNull(message = "Le champ role ne peut pas être null")
        UserRole role
) {
}
