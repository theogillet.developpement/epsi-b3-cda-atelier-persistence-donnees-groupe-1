package com.grandbazar.api.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import java.util.Date;

public record RatingDTO(
        @NotNull(message = "Seller ID cannot be null")
        Integer sellerId,

        @NotNull(message = "Buyer ID cannot be null")
        Integer buyerId,

        @NotNull(message = "Rating cannot be null")
        @Min(value = 1, message = "Rating must be at least 1")
        @Max(value = 5, message = "Rating must be at most 5")
        Integer rating,

        @NotNull(message = "Comment cannot be null")
        String comment,

        Date createdAt
) {}
