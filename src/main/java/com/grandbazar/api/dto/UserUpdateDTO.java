package com.grandbazar.api.dto;

import com.grandbazar.api.type.UserRole;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record UserUpdateDTO(
        @NotBlank(message = "Le champ username ne peut pas être vide")
        String username,

        @NotBlank(message = "Le champ password ne peut pas être vide")
        String password,

        @NotBlank(message = "Le champ email ne peut pas être vide")
        @Email(message = "Le champ email doit être une adresse email valide")
        String email,

        @NotNull(message = "Le champ role ne peut pas être null")
        UserRole role
) {
}
