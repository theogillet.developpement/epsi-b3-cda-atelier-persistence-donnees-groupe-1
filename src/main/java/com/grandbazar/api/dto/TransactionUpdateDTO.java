package com.grandbazar.api.dto;

import java.util.Date;

public record TransactionUpdateDTO(
        Integer productId,

        Integer buyerId,

        Date createdAt
) {}
