package com.grandbazar.api.dto;

import com.grandbazar.api.type.ProductCategory;
import com.grandbazar.api.type.ProductStatus;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.util.Date;

public record ProductDTO(
        @NotBlank(message = "Le champ name ne peut pas être vide")
        @NotNull(message = "Le champ name ne peut pas être null")
        String name,
        @NotBlank(message = "Le champ description ne peut pas être vide")
        @NotNull(message = "Le champ description ne peut pas être null")
        String description,
        @NotNull(message = "Le champ price ne peut pas être null")
        Double price,
        @NotNull(message = "Le champ category ne peut pas être null")
        ProductCategory category,
        @NotNull(message = "Le champ status ne peut pas être null")
        ProductStatus status,

        @NotNull(message = "Le champ seller_id ne peut pas être null")
        Integer seller_id,

        @NotNull(message = "Le champ createdDate ne peut pas être null")
        Date createdAt
) {

}
