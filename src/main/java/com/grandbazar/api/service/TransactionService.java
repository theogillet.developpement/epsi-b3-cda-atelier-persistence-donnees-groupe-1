package com.grandbazar.api.service;

import com.grandbazar.api.dto.TransactionDTO;
import com.grandbazar.api.dto.TransactionUpdateDTO;
import com.grandbazar.api.entity.ProductEntity;
import com.grandbazar.api.entity.TransactionEntity;
import com.grandbazar.api.entity.UserEntity;
import com.grandbazar.api.repository.ProductRepository;
import com.grandbazar.api.repository.TransactionRepository;
import com.grandbazar.api.repository.UserRepository;
import com.grandbazar.api.type.ProductStatus;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TransactionService {

    private final TransactionRepository transactionRepository;
    private final UserRepository userRepository;
    private final ProductRepository productRepository;

    public TransactionService(TransactionRepository transactionRepository, UserRepository userRepository, ProductRepository productRepository) {
        this.transactionRepository = transactionRepository;
        this.userRepository = userRepository;
        this.productRepository = productRepository;
    }

    public List<TransactionDTO> getAllTransactions() {
        List<TransactionEntity> transactionEntities = transactionRepository.findAll();
        return transactionEntities.stream()
                .map(this::mapToDTO)
                .collect(Collectors.toList());
    }

    public TransactionDTO getTransactionById(Integer id) {
        Optional<TransactionEntity> transactionEntityOptional = transactionRepository.findById(id);
        if (transactionEntityOptional.isPresent()) {
            TransactionEntity transactionEntity = transactionEntityOptional.get();
            return mapToDTO(transactionEntity);
        } else {
            throw new RuntimeException("Transaction with id " + id + " not found");
        }
    }

    public TransactionDTO createTransaction(TransactionDTO transactionDTO) {
        TransactionEntity transactionEntity = mapToEntity(transactionDTO);
        transactionEntity = transactionRepository.save(transactionEntity);

        Optional<ProductEntity> optionalProductEntity = productRepository.findById(transactionDTO.productId());
        if (optionalProductEntity.isPresent()) {
            ProductEntity productEntity = optionalProductEntity.get();
            productEntity.setStatus(ProductStatus.SOLD);
            productRepository.save(productEntity);
        } else {
            throw new EntityNotFoundException("Product not found with ID: " + transactionDTO.productId());
        }

        return mapToDTO(transactionEntity);
    }

    public TransactionDTO updateTransaction(Integer id, TransactionUpdateDTO transactionUpdateDTO) {
        Optional<TransactionEntity> transactionEntityOptional = transactionRepository.findById(id);
        if (transactionEntityOptional.isPresent()) {
            TransactionEntity transactionEntity = transactionEntityOptional.get();
            transactionRepository.save(transactionEntity);
            return mapToDTO(transactionEntity);
        } else {
            throw new RuntimeException("Transaction with id " + id + " not found");
        }
    }

    public void deleteTransaction(Integer id) {
        if (transactionRepository.existsById(id)) {
            transactionRepository.deleteById(id);
        } else {
            throw new RuntimeException("Transaction with id " + id + " not found");
        }
    }

    private TransactionDTO mapToDTO(TransactionEntity transactionEntity) {
        return new TransactionDTO(
                transactionEntity.getProduct().getId(),
                transactionEntity.getBuyer().getId(),
                transactionEntity.getCreatedAt()
        );
    }

    private TransactionEntity mapToEntity(TransactionDTO transactionDTO) {
        ProductEntity productEntity = productRepository.findById(transactionDTO.productId())
                .orElseThrow(() -> new RuntimeException("Product with id " + transactionDTO.productId() + " not found"));
        UserEntity buyerEntity = userRepository.findById(transactionDTO.buyerId())
                .orElseThrow(() -> new RuntimeException("User with id " + transactionDTO.buyerId() + " not found"));

        return new TransactionEntity(productEntity, buyerEntity);
    }


}
