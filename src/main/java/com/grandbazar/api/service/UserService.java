package com.grandbazar.api.service;

import com.grandbazar.api.dto.LoginDTO;
import com.grandbazar.api.dto.UserDTO;
import com.grandbazar.api.dto.UserUpdateDTO;
import com.grandbazar.api.entity.UserEntity;
import com.grandbazar.api.repository.UserRepository;
import jakarta.validation.Valid;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    private final UserRepository userRepository;

    private final BCryptPasswordEncoder passwordEncoder;

    public UserService(UserRepository userRepository, BCryptPasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public List<UserEntity> getAllUsers() {
        return userRepository.findAll();
    }

    public  UserEntity getUserByEmailandPassword(LoginDTO login){

        return userRepository.findByEmail(login.email())
                .filter(user -> {
                    String hashedPassword = DigestUtils.sha256Hex(login.password());
                    return hashedPassword.equals(user.getPassword());
                })
                .orElse(null);
    }
    public boolean createUser(@Valid UserDTO userDTO) {
        UserEntity user = setUser(userDTO);
        Optional<UserEntity> userByEmail = userRepository.findByEmail(user.getEmail());
        if (userByEmail.isPresent()) {
            return false;
        }
        userRepository.save(user);
        return true;
    }


    public Boolean updateUser(int id, UserUpdateDTO userUpdateDTO) {

        return userRepository.findById(id)
                .map(user -> {
                    if (userUpdateDTO.username() != null && !userUpdateDTO.username().isBlank()) {
                        user.setUsername(userUpdateDTO.username());
                    }
                    if (userUpdateDTO.password() != null && !userUpdateDTO.password().isBlank()) {
                        String hashedPassword = DigestUtils.sha256Hex(userUpdateDTO.password());
                        user.setPassword(hashedPassword);
                    }
                    if (userUpdateDTO.email() != null && !userUpdateDTO.email().isBlank()) {
                        user.setEmail(userUpdateDTO.email());
                    }
                    if (userUpdateDTO.role() != null) {
                        user.setRole(userUpdateDTO.role());
                    }
                    userRepository.save(user);
                    return true;
                }).orElseThrow(() -> new IllegalArgumentException("Le user avec l'ID donné n'existe pas."));
    }

    private UserEntity setUser(UserDTO userDTO) {
        String hashedPassword = DigestUtils.sha256Hex(userDTO.password());
        UserEntity user = new UserEntity(
                userDTO.username(),
                userDTO.email(),
                hashedPassword,
                userDTO.role()
        );
        return user;
    }

}
