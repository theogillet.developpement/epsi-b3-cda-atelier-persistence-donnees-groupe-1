package com.grandbazar.api.service;

import com.grandbazar.api.dto.RatingDTO;
import com.grandbazar.api.dto.RatingUpdateDTO;
import com.grandbazar.api.entity.RatingEntity;
import com.grandbazar.api.entity.UserEntity;
import com.grandbazar.api.repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class RatingService {

    private final UserRepository userRepository;

    public RatingService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public RatingDTO mapToDTO(RatingEntity ratingEntity) {
        return new RatingDTO(
                ratingEntity.getSeller().getId(),
                ratingEntity.getBuyer().getId(),
                ratingEntity.getRating(),
                ratingEntity.getComment(),
                ratingEntity.getCreatedAt()
        );
    }

    public RatingEntity mapToEntity(RatingDTO ratingDTO) {
        UserEntity seller = userRepository.findById(ratingDTO.sellerId()).orElseThrow();
        UserEntity buyer = userRepository.findById(ratingDTO.buyerId()).orElseThrow();

        return new RatingEntity(
                seller,
                buyer,
                ratingDTO.rating(),
                ratingDTO.comment()
        );
    }

    public void updateEntityFromDTO(RatingEntity ratingEntity, RatingUpdateDTO ratingUpdateDTO) {
        if (ratingUpdateDTO.sellerId() != null) {
            ratingEntity.setSeller(userRepository.findById(ratingUpdateDTO.sellerId()).orElseThrow());
        }
        if (ratingUpdateDTO.buyerId() != null) {
            ratingEntity.setBuyer(userRepository.findById(ratingUpdateDTO.buyerId()).orElseThrow());
        }
        if (ratingUpdateDTO.rating() != null) {
            ratingEntity.setRating(ratingUpdateDTO.rating());
        }
        if (ratingUpdateDTO.comment() != null) {
            ratingEntity.setComment(ratingUpdateDTO.comment());
        }
        if (ratingUpdateDTO.createdAt() != null) {
            ratingEntity.setCreatedAt(ratingUpdateDTO.createdAt());
        }
    }

}
