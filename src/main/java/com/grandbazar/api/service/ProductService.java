package com.grandbazar.api.service;

import com.grandbazar.api.dto.ProductDTO;
import com.grandbazar.api.dto.ProductUpdateDTO;
import com.grandbazar.api.entity.ProductEntity;
import com.grandbazar.api.repository.ProductRepository;
import com.grandbazar.api.type.ProductCategory;
import com.grandbazar.api.type.ProductStatus;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import org.springframework.data.jpa.domain.Specification;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public ProductEntity getProductById(Integer id) {
        return productRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("The product with " + id + " was not found"));
    }

    public void createProduct(@Valid ProductDTO productDTO) {
        ProductEntity product = toEntity(productDTO);
        productRepository.save(product);
    }

    public void deleteProduct(Integer idProduct, Integer idUser) {
        ProductEntity product = productRepository.findById(idProduct)
                .orElseThrow(() -> new IllegalArgumentException("The product with the given ID does not exist."));

        if (!product.getSellerId().equals(idUser)) {
            throw new SecurityException("You are not authorized to delete this product.");
        }

        productRepository.delete(product);
    }

    public Boolean updateProduct(int id, ProductUpdateDTO productUpdateDTO) {
        return productRepository.findById(id).map(product -> {
            if (!product.getSellerId().equals(productUpdateDTO.seller_id())) {
                throw new SecurityException("You are not authorized to update this product.");
            }
            if (productUpdateDTO.name() != null) {
                product.setName(productUpdateDTO.name());
            }
            if (productUpdateDTO.description() != null) {
                product.setDescription(productUpdateDTO.description());
            }
            if (productUpdateDTO.price() != null) {
                product.setPrice(productUpdateDTO.price());
            }
            if (productUpdateDTO.category() != null) {
                product.setCategory(productUpdateDTO.category());
            }
            if (productUpdateDTO.createdAt() != null) {
                product.setCreatedAt(productUpdateDTO.createdAt());
            }
            productRepository.save(product);
            return true;
        }).orElseThrow(() -> new IllegalArgumentException("Given product ID does not match any product."));
    }

    public Optional<ProductEntity> getProductByName(String name) {
        return productRepository.findByName(name);
    }

    private ProductEntity toEntity(ProductDTO dto) {
        return new ProductEntity(
                dto.name(),
                dto.description(),
                dto.price(),
                dto.createdAt(),
                dto.category(),
                dto.status(),
                dto.seller_id()
        );
    }

    public List<ProductEntity> getAllProducts() {
        return productRepository.findAll();
    }

    public List<ProductEntity> searchProducts(String name, String description, Double minPrice, Double maxPrice, ProductCategory category) {
        Specification<ProductEntity> specification = Specification.where(null);

        if (name != null && !name.isEmpty()) {
            specification = specification.and((root, query, criteriaBuilder) ->
                    criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), "%" + name.toLowerCase() + "%"));
        }

        if (description != null && !description.isEmpty()) {
            specification = specification.and((root, query, criteriaBuilder) ->
                    criteriaBuilder.like(criteriaBuilder.lower(root.get("description")), "%" + description.toLowerCase() + "%"));
        }

        if (minPrice != null) {
            specification = specification.and((root, query, criteriaBuilder) ->
                    criteriaBuilder.greaterThanOrEqualTo(root.get("price"), minPrice));
        }

        if (maxPrice != null) {
            specification = specification.and((root, query, criteriaBuilder) ->
                    criteriaBuilder.lessThanOrEqualTo(root.get("price"), maxPrice));
        }

        if (category != null) {
            specification = specification.and((root, query, criteriaBuilder) ->
                    criteriaBuilder.equal(root.get("category"), category));
        }

        specification = specification.and((root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("status"), ProductStatus.AVAILABLE));

        return productRepository.findAll(specification);
    }

}
