package com.grandbazar.api.repository;

import com.grandbazar.api.entity.ProductEntity;
import com.grandbazar.api.type.ProductCategory;
import com.grandbazar.api.type.ProductStatus;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository  extends JpaRepository<ProductEntity, Integer> {

    Optional<ProductEntity> findByName(String name);

    List<ProductEntity> findBySellerId(int id);

    List<ProductEntity> findAll(Specification<ProductEntity> specification);

}
