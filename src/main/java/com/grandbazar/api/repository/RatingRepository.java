package com.grandbazar.api.repository;

import com.grandbazar.api.entity.RatingEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RatingRepository extends JpaRepository<RatingEntity, Integer> {

    List<RatingEntity> findBySellerId(Integer idSeller);

    List<RatingEntity> findByBuyerId(Integer idBuyer);

    @Query("SELECT r FROM RatingEntity r WHERE r.buyer.id = :idUser OR r.seller.id = :idUser")
    List<RatingEntity> findByBuyerIdOrSellerId(Integer idUser);
}
