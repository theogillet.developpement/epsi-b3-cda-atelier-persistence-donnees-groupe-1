package com.grandbazar.api.repository;

import com.grandbazar.api.entity.TransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransactionRepository extends JpaRepository<TransactionEntity, Integer> {

    List<TransactionEntity> findByProductId(Integer idProduct);

    List<TransactionEntity> findByBuyerId(Integer idBuyer);
}
