package com.grandbazar.api.controller;

import com.grandbazar.api.dto.ProductDTO;
import com.grandbazar.api.dto.ProductUpdateDTO;
import com.grandbazar.api.entity.ProductEntity;
import com.grandbazar.api.entity.TransactionEntity;
import com.grandbazar.api.repository.RatingRepository;
import com.grandbazar.api.repository.TransactionRepository;
import com.grandbazar.api.service.ProductService;
import com.grandbazar.api.type.ProductCategory;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/products")
public class ProductController {

    private final ProductService productService;
    private final TransactionRepository transactionRepository;
    private final RatingRepository ratingRepository;

    public ProductController(ProductService productService, TransactionRepository transactionRepository, RatingRepository ratingRepository) {
        this.productService= productService;
        this.transactionRepository = transactionRepository;
        this.ratingRepository = ratingRepository;
    }

    @GetMapping
    public ResponseEntity<List<ProductEntity>> getAllProducts() {
        List<ProductEntity> products = productService.getAllProducts();
        if (products.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(products);
    }

    @PostMapping
    public ResponseEntity<Void> createProduct(@RequestBody @Valid ProductDTO productDTO){
        productService.createProduct(productDTO);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductEntity> getProduct(@PathVariable String name) {
        Optional<ProductEntity> productEntity = productService.getProductByName(name);
        return productEntity.map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @Transactional
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable Integer idProduct, @RequestBody Integer userId) {
        List<TransactionEntity> transactions = transactionRepository.findByProductId(idProduct);
        transactionRepository.deleteAll(transactions);

        productService.deleteProduct(idProduct, userId);

        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Void> updateProduct(@PathVariable int id, @RequestBody @Valid ProductUpdateDTO productUpdateDTO) {
        boolean updated = productService.updateProduct(id, productUpdateDTO);
        if (updated) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/search")
    public ResponseEntity<List<ProductEntity>> searchProducts(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String description,
            @RequestParam(required = false) Double minPrice,
            @RequestParam(required = false) Double maxPrice,
            @RequestParam(required = false) ProductCategory category) {

        List<ProductEntity> products = productService.searchProducts(name, description, minPrice, maxPrice, category);
        if (products.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(products);
    }

}
