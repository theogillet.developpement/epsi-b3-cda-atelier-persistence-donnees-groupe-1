package com.grandbazar.api.controller;

import com.grandbazar.api.dto.LoginDTO;
import com.grandbazar.api.dto.UserDTO;
import com.grandbazar.api.dto.UserUpdateDTO;
import com.grandbazar.api.entity.ProductEntity;
import com.grandbazar.api.entity.RatingEntity;
import com.grandbazar.api.entity.TransactionEntity;
import com.grandbazar.api.entity.UserEntity;
import com.grandbazar.api.repository.ProductRepository;
import com.grandbazar.api.repository.RatingRepository;
import com.grandbazar.api.repository.TransactionRepository;
import com.grandbazar.api.repository.UserRepository;
import com.grandbazar.api.service.UserService;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;
    private final UserRepository userRepository;
    private final TransactionRepository transactionRepository;
    private final RatingRepository ratingRepository;
    private final ProductRepository productRepository;

    public UserController(UserService userService, UserRepository userRepository, TransactionRepository transactionRepository, RatingRepository ratingRepository, ProductRepository productRepository) {
        this.userService = userService;
        this.userRepository = userRepository;
        this.transactionRepository = transactionRepository;
        this.ratingRepository = ratingRepository;
        this.productRepository = productRepository;
    }

    @GetMapping
    public ResponseEntity<List<UserEntity>> getAllUsers() {
        List<UserEntity> users = userService.getAllUsers();
        if (users.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(users);
    }

    @PostMapping
    public ResponseEntity<String> createUser(@RequestBody @Valid UserDTO userDTO) {
        boolean created = userService.createUser(userDTO);
        if (created) {
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } else {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Email already taken.");
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<UserEntity>> getUser(@PathVariable int id) {
        Optional<UserEntity> user = userRepository.findById(id);
        if (user.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(user);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Void> updateUser(@PathVariable int id, @RequestBody UserUpdateDTO userUpdateDTO) {
        boolean updated = userService.updateUser(id, userUpdateDTO);
        if (updated) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @Transactional
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable int id) {
        List<ProductEntity> products = productRepository.findBySellerId(id);
        for (ProductEntity product : products) {
            List<TransactionEntity> transactions = transactionRepository.findByProductId(product.getId());
            transactionRepository.deleteAll(transactions);
            productRepository.delete(product);
        }

        List<RatingEntity> ratings = ratingRepository.findByBuyerIdOrSellerId(id);
        ratingRepository.deleteAll(ratings);

        List<TransactionEntity> transactions = transactionRepository.findByBuyerId(id);
        transactionRepository.deleteAll(transactions);

        userRepository.deleteById(id);

        return ResponseEntity.noContent().build();
    }

    @PostMapping("/login")
    public ResponseEntity<UserEntity> login(@RequestBody LoginDTO loginDTO) {
        UserEntity user = userService.getUserByEmailandPassword(loginDTO);
        if (user == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.ok(user);

    }

    @PostMapping("/register")
    public ResponseEntity<String> register(@RequestBody @Valid UserDTO userDTO) {
        boolean created = userService.createUser(userDTO);
        if (created) {
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } else {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Email already taken.");
        }
    }


}
