package com.grandbazar.api.controller;

import com.grandbazar.api.dto.RatingDTO;
import com.grandbazar.api.dto.RatingUpdateDTO;
import com.grandbazar.api.entity.RatingEntity;
import com.grandbazar.api.repository.RatingRepository;
import com.grandbazar.api.service.RatingService;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/ratings")
public class RatingController {

    private final RatingRepository ratingRepository;
    private final RatingService ratingService;

    public RatingController(RatingRepository ratingRepository, RatingService ratingService) {
        this.ratingRepository = ratingRepository;
        this.ratingService = ratingService;
    }

    @GetMapping
    public ResponseEntity<List<RatingDTO>> getAllRatings() {
        List<RatingEntity> ratingEntities = ratingRepository.findAll();
        List<RatingDTO> ratingDTOs = ratingEntities.stream()
                .map(ratingService::mapToDTO)
                .collect(Collectors.toList());
        return ResponseEntity.ok(ratingDTOs);
    }

    @GetMapping("/{id}")
    public ResponseEntity<RatingDTO> getRatingById(@PathVariable("id") Integer id) {
        Optional<RatingEntity> ratingEntityOptional = ratingRepository.findById(id);
        if (ratingEntityOptional.isPresent()) {
            RatingEntity ratingEntity = ratingEntityOptional.get();
            RatingDTO ratingDTO = ratingService.mapToDTO(ratingEntity);
            return ResponseEntity.ok(ratingDTO);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity<Void> createRating(@Valid @RequestBody RatingDTO ratingDTO) {
        RatingEntity ratingEntity = ratingService.mapToEntity(ratingDTO);
        ratingRepository.save(ratingEntity);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<RatingDTO> updateRating(@PathVariable("id") Integer id, @Valid @RequestBody RatingUpdateDTO ratingUpdateDTO) {
        Optional<RatingEntity> ratingEntityOptional = ratingRepository.findById(id);
        if (ratingEntityOptional.isPresent()) {
            RatingEntity ratingEntity = ratingEntityOptional.get();
            ratingService.updateEntityFromDTO(ratingEntity, ratingUpdateDTO);
            RatingEntity updatedRating = ratingRepository.save(ratingEntity);
            RatingDTO updatedRatingDTO = ratingService.mapToDTO(updatedRating);
            return ResponseEntity.ok(updatedRatingDTO);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteRating(@PathVariable("id") Integer id) {
        if (ratingRepository.existsById(id)) {
            ratingRepository.deleteById(id);
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

}
