package com.grandbazar.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GrandBazarApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GrandBazarApiApplication.class, args);
	}

}
